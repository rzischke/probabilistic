#ifndef PROBABILISTIC_R_MODELLING_COVID_FIT_HPP_GUARD
#define PROBABILISTIC_R_MODELLING_COVID_FIT_HPP_GUARD

#include <Rinternals.h>
#include <dll_visibility.h>

extern "C" {

    DLL_PUBLIC SEXP R_covid_forward(
        SEXP model_R,
        SEXP covid_R,
        SEXP cumulative_cases_index_R,
        SEXP daily_cases_index_R,
        SEXP population_index_R
    );

    DLL_PUBLIC SEXP R_covid_fits(
        SEXP models_R,
        SEXP scoring_rule_R,
        SEXP covid_R,
        SEXP cumulative_cases_index_R,
        SEXP daily_cases_index_R,
        SEXP population_index_R,
        SEXP learning_rate_R,
        SEXP maximum_optimiser_iterations_R,
        SEXP timeout_in_seconds_R
    );
}
#endif

