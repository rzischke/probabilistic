FROM r-base AS probabilistic-base
WORKDIR /root/
RUN apt-get update && apt-get install -y \
    build-essential \
    libxml2-dev \
    libcurl4-openssl-dev \
    libssl-dev && \
rm -rf /var/lib/apt/lists/*

FROM probabilistic-base AS builder
WORKDIR /root/
RUN Rscript --vanilla -e "install.packages(c(\
    'distributional',\
    'fable',\
    'gridExtra',\
    'lemon',\
    'tibble',\
    'tsibble',\
    'xptr',\
    'zoo'\
), dependencies = TRUE, repos = 'https://cran.rstudio.com/')"
RUN apt-get update && apt-get install -y \
    cmake \
    file \
    git \
    python3-dev \
    python3-pip && \
rm -rf /var/lib/apt/lists/*
COPY ./ probabilistic/
WORKDIR probabilistic/
RUN Rscript --vanilla "download_external_dependencies.R"
RUN BUILD=Release PROBABILISTIC_SILENCE_WARNS=TRUE R CMD INSTALL probabilistic
WORKDIR /root/
RUN rm -rf probabilistic 

FROM probabilistic-base
COPY --from=builder /usr/lib/R/site-library/ /usr/lib/R/site-library/
COPY --from=builder /usr/local/lib/R/site-library/ /usr/lib/R/site-library/

